# CAT - Dossier
This repository is a demo how to upload and display results from an analysis repository to a Dossier.
Here we receive output from the [Continuous Analysis Template](https://gitlab.cern.ch/sneubert/ContinuousAnalysisTemplate/)

*Created from https://gitlab.cern.ch/austyuzh/ContinuousAnalysisTemplate/commits/29a88853e46b9a5d1b40c69be991382acbe5b862 on branch master*

# Slides
[Slides](latex/CAT.pdf)

# Plots

![Xfit](plots/Xfit.png)